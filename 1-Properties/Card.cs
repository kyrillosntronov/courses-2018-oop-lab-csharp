﻿using System;

namespace Properties {

    public class Card
    {
        private readonly string seed;
        private readonly string name;
        private readonly int ordial;

        public Card(string name, string seed, int ordial)
        {
            this.name = name;
            this.ordial = ordial;
            this.seed = seed;
        }

        internal Card(Tuple<string, string, int> tuple)
            : this(tuple.Item1, tuple.Item2, tuple.Item3) { }

        public string Seed
        {
            get { return this.seed; }
        }

        public string Name
        {
            get { return this.name; }
        }

        public int Ordinal
        {
            get { return this.ordial; }
        }

        public override string ToString()
        {
            return $"{this.GetType().Name}(Name={this.Name}, Seed={this.Seed}, Ordinal={this.Ordinal})";
        }

        public override bool Equals(object obj)
        {
            return this.name == ((Card)obj).name && this.seed == ((Card)obj).seed && this.ordial == ((Card)obj).ordial;
        }

        public override int GetHashCode()
        {
            return this.name.GetHashCode() + this.ordial.GetHashCode() + this.seed.GetHashCode();
        }
    }

}